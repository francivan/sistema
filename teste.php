<?php

require_once __DIR__ . '/vendor/autoload.php';

use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;
include './banco/conexao.php';
$msg = 'teste';
$notifications = [];
$sql = "SELECT a.endpoint, a.public_key, a.auth_token, b.nome "
        . "FROM permissao_push a, usuarios b "
        . "WHERE a.id_cliente = b.id_cliente AND a.id_cliente=9";

$clientenotificacao = $conexao->query($sql);
$permissaoPush = $clientenotificacao->fetch(PDO::FETCH_ASSOC);

if ($permissaoPush) {
    $notifications[] = [
        'subscription' => Subscription::create([
            'endpoint' => $permissaoPush['endpoint'],
            'publicKey' => $permissaoPush['public_key'],
            'authToken' => $permissaoPush['auth_token']
        ]),
        'payload' => '{"title":"' . $permissaoPush['nome'] . '","msg":"' . $msg . '","icon":"/sistema/public/images/icon.png","badge":"/sistema/public/images/badge.png","url":"https://developers.google.com/web/"}'
    ];
}

$auth = [
    'VAPID' => [
        'subject' => 'mailto:francivan.castro@gmail.com',
        'publicKey' => 'BC08dSbsY31wE3w1yCdWVyvbQiNTKZ-uIjXEzqrx23-gJv1EWc_Ny3SXMp7hDwfMlAFc23wlnj_nPXuW1Qrs0sM',
        'privateKey' => 'JZ2Lfgw343CRuX2IX35S2Cf0ZNGQePsgeJEPFIVwhMc'
    ],
];
$webPush = new WebPush($auth);

foreach ($notifications as $notification) {
    $webPush->queueNotification(
            $notification['subscription'],
            $notification['payload']
    );
}
foreach ($webPush->flush() as $report) {
    $endpoint = $report->getRequest()->getUri()->__toString();
    if ($report->isSuccess()) {
        echo "[v] Message sent successfully for subscription {$endpoint}.";
    } else {
        echo "[x] Message failed to sent for subscription {$endpoint}: {$report->getReason()}";
    }
}