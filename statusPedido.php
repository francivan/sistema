<?php

require_once __DIR__ . '/vendor/autoload.php';

use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;

session_start();

if (empty($_SESSION['id'])) {
    header('location:/sistema/login.php');
}
include './banco/conexao.php';
if (isset($_GET['ticket']) && isset($_GET['status'])) {
    $ticket = $_GET['ticket'];
    $status = $_GET['status'];
    $id = $_GET['id'];

    try {
        $alterar = $conexao->query("UPDATE vendas set status = '$status' where ticket = '$ticket'");
        $msg = getMensagemPorStatus($status);
        $notifications = [];
        $sql = "SELECT a.endpoint, a.public_key, a.auth_token, b.nome "
                . "FROM permissao_push a, usuarios b "
                . "WHERE a.id_cliente = b.id_cliente AND a.id_cliente=" . $id;

        $clientenotificacao = $conexao->query($sql);
        $permissaoPush = $clientenotificacao->fetch(PDO::FETCH_ASSOC);

        if ($permissaoPush) {
            $notifications[] = [
                'subscription' => Subscription::create([
                    'endpoint' => $permissaoPush['endpoint'],
                    'publicKey' => $permissaoPush['public_key'],
                    'authToken' => $permissaoPush['auth_token']
                ]),
                'payload' => '{"title":"' . $permissaoPush['nome'] . '","msg":"' . $msg . '","icon":"/sistema/public/images/icon.png","badge":"/sistema/public/images/badge.png","url":"https://developers.google.com/web/"}'
            ];
        }
        $auth = [
            'VAPID' => [
                'subject' => 'mailto:francivan.castro@gmail.com',
                'publicKey' => 'BC08dSbsY31wE3w1yCdWVyvbQiNTKZ-uIjXEzqrx23-gJv1EWc_Ny3SXMp7hDwfMlAFc23wlnj_nPXuW1Qrs0sM',
                'privateKey' => 'JZ2Lfgw343CRuX2IX35S2Cf0ZNGQePsgeJEPFIVwhMc'
            ],
        ];
        $webPush = new WebPush($auth);
        foreach ($notifications as $notification) {
            $webPush->queueNotification(
                    $notification['subscription'],
                    $notification['payload']
            );
        }
        foreach ($webPush->flush() as $report) {
            $endpoint = $report->getRequest()->getUri()->__toString();
            if ($report->isSuccess()) {
                // echo "[v] Message sent successfully for subscription {$endpoint}.";
            } else {
                // implementar log no futuro
                //echo "[x] Message failed to sent for subscription {$endpoint}: {$report->getReason()}";                
            }
        }
        header('location:/sistema/ticket.php?ticket=' . $ticket);
    } catch (Exception $e) {
        echo 'Problemas na Query';
    }
} else {
    header('location:/sistema/index.php');
}

function getMensagemPorStatus($status_pedido) {
    switch ($status_pedido) {
        case 'C':
            $siglaStatus = "C";
            $status_pedido = 'Seu pedido foi Confirmado e está sendo preparado.';
            break;
        case 'E':
            $siglaStatus = "E";
            $status_pedido = 'Saiu para Entrega';
            break;
        case 'F':
            $siglaStatus = "F";
            $status_pedido = 'Obrigado por comprar com a gente! - Volte sempre!';
            break;
    }
    return $status_pedido;
}
