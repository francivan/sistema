<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="/sistema/public/css/reset.css"> <!-- CSS reset -->
        <link rel="stylesheet" href="/sistema/public/css/style.css"> <!-- Gem style -->
        <script src="/sistema/public/js/modernizr.js"></script> <!-- Modernizr -->        
        <link rel="manifest" href="manifest.json">
        <link rel="icon" type="image/png" sizes="144x144"  href="/sistema/public/images/Icon-144.png">
        <link href="/sistema/public/images/Icon-144.png" rel="shortcut icon" type="image/vnd.microsoft.icon">
        <script src="/sistema/public/js/webapp.js"></script>
    </head>
    <body>
        <?php
        include './banco/conexao.php';
        include './template/nav.php';
        ?>
        <main class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 text-center">
                    <h2> Pedido Realizado com Sucesso! Ticket do Pedido: <?php echo $ticket; ?></h2> <br> <h4><a href="./ticket.php?ticket=<?php echo $ticket; ?>">Clique aqui para Acompanhar o Andamento do Pedido</a></h4>
                </div>
            </div>
        </main>
        <?php include './template/rodape.html' ?>
    </body>
</html>
