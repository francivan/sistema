<?php
session_start();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Forma de Pagamento</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="/sistema/public/css/reset.css"> <!-- CSS reset -->
        <link rel="stylesheet" href="/sistema/public/css/style.css"> <!-- Gem style -->
        <script src="/sistema/public/js/modernizr.js"></script> <!-- Modernizr -->        
        <link rel="manifest" href="manifest.json">
        <link rel="icon" type="image/png" sizes="144x144"  href="/sistema/public/images/Icon-144.png">
        <link href="/sistema/public/images/Icon-144.png" rel="shortcut icon" type="image/vnd.microsoft.icon">
        <script src="/sistema/public/js/webapp.js"></script>
        <script src="jquery.mask.js"></script>
        <style>
            .navbar{
                margin-bottom: 0;
            }
        </style>
        <script>
            $(document).ready(function () {
                $("#cep").mask("00 000-000");
            });
        </script>
    </head>
    <body>
        <?php
        include './banco/conexao.php';
        include './template/nav.php';
        ?>
        <main class="container">
            <?php if (empty($_SESSION['id'])) { ?>
            <div class="alert alert-warning">
                Para ter acesso a pagamento online é necessário logar no sistema. 
                <a href="./login.php" class="btn btn-sm btn-default">
                    <span class="glyphicon glyphicon-log-in"> Login</span>
                </a>
            </div>
            <?php } ?>
            <div class="col-sm-6 col-sm-offset-3">
                <form method="post" action="./inserirFormaPagto.php">
                    <label for="forma">Forma de Pagamento</label>
                    <select class="form-control" name="forma" id="forma" required>
                        <option value="ED"> Na Entrega Dinheiro</option>
                        <option value="EC"> Na Entrega Cartão</option>
                        <?php if (!empty($_SESSION['id'])) { ?>                    
<!--                            <option value="O"> Pagamento Online </option>-->
                        <?php } ?>
                    </select>
                    <br>
                    <br>		
                    <button type="submit" class="btn btn-lg btn-default">
                        <span class="glyphicon glyphicon-ok"> Confirmar</span>
                    </button>
                </form>
            </div>
        </main>
        <?php include './template/rodape.html' ?>
    </body>
</html>