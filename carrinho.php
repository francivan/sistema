<?php
ob_start();
session_start();
//if (empty($_SESSION['id'])) {
//    header('location:login.php');
//}
?>
<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Carrinho</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="/sistema/public/css/reset.css"> <!-- CSS reset -->
        <link rel="stylesheet" href="/sistema/public/css/style.css"> <!-- Gem style -->
        <script src="/sistema/public/js/modernizr.js"></script> <!-- Modernizr -->        
        <link rel="manifest" href="manifest.json">
        <link rel="icon" type="image/png" sizes="144x144"  href="/sistema/public/images/Icon-144.png">
        <link href="/sistema/public/images/Icon-144.png" rel="shortcut icon" type="image/vnd.microsoft.icon">
        <script src="/sistema/public/js/webapp.js"></script>
    </head>
    <body>	
        <?php
        include './banco/conexao.php';
        include './template/nav.php';
        
        ?>
        <main class="container">
            <form method="post" action="./finalizarCompra.php">
                <?php
                if (!empty($_GET['id'])) {
                    $id_prod = $_GET['id'];
                    $action = (isset($_GET['action'])) ? $_GET['action'] : null;
                    if ($action) {
                        if ($action == 'adicionar') {
                            if (isset($_SESSION['carrinho'][$id_prod])) {
                                $_SESSION['carrinho'][$id_prod] += 1;
                            } else {
                                $_SESSION['carrinho'][$id_prod] = 1;
                            }
                            header('Location: carrinho.php');
                            exit();
                        }
                        if ($action == 'remover') {
                            if (isset($_SESSION['carrinho'][$id_prod])) {
                                $_SESSION['carrinho'][$id_prod] -= 1;
                            } else {
                                $_SESSION['carrinho'][$id_prod] = 1;
                            }
                            header('Location: carrinho.php');
                            exit();
                        }
                    }
                } 
                include 'mostraCarrinho.php';
                if (!empty($_SESSION['id']) && $exibe_user['valor']) {
                    $total += $exibe_user['valor'];
                } elseif (!isset($_SESSION['id']) && isset($_SESSION['frete'])) {
                    $total += $_SESSION['frete'];
                }
                ?>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h1>Total: R$ <span id="total_pagar"><?php echo number_format($total, 2, ',', '.'); ?></span></h1>
                    </div>
                    <div class="panel-body">
                        <a href="index.php"  class="btn btn-primary">Continuar Comprando</a>
                        <?php if (count($_SESSION['carrinho']) > 0) { ?>
                            <button  type="submit" class="btn btn-success">Finalizar Compra</button>
                        <?php } ?>
                    </div>
                </div>
            </form>
        </main>
        <script src="public/js/jquery-2.1.1.js"></script>
        <script src="public/js/jquery.mixitup.min.js"></script>
        <script src="public/js/main.js"></script> <!-- Resource jQuery -->
        <!-- FIM DO MENU FOOD -->
        <script src="/sistema/public/js/main.js"></script> <!-- Gem jQuery -->
        <?php include './template/cart.php'; ?>
        <?php
        include './template/rodape.html';
        ?>
    </body>
</html>
<?php ob_end_flush(); ?>