<?php

session_start();
include './banco/conexao.php';

if (!empty($_POST)) {
    // limpo a sessão
    limpaSessao();
    $_SESSION['cidade'] = $_POST['cidade'];
    $_SESSION['id_frete'] = $_POST['id_frete'];
    $_SESSION['nome_comprador'] = $_POST['nome_comprador'];
    $_SESSION['endereco'] = $_POST['endereco'];
}

$msgwpp = "";
$total = null;
if ($_SESSION['forma'] == '') {
    header('location:formFormaPagto.php');
    exit();
} else {
    $data = date('Y-m-d');
    $ticket = uniqid();
    $id_user = (isset($_SESSION['id'])) ? $_SESSION['id'] : 'null';
    $formapagto = (isset($_SESSION['forma'])) ? $_SESSION['forma'] : 'null';
    $id_frete = ($_SESSION['id_frete']) ? $_SESSION['id_frete'] : 'null';
    $comprador = ($_SESSION['nome_comprador']) ? $_SESSION['nome_comprador'] : 'null';
    $endereco = ($_SESSION['endereco']) ? $_SESSION['endereco'] : 'null';
    $msgwpp .= '_*ID PEDIDO*_' . "%0A";
    $msgwpp .= $ticket . "%0A";
    $msgwpp .= '_*PRODUTOS*_' . "%0A";
    $buscafrete = $conexao->query("SELECT * FROM frete WHERE id_frete='$id_frete'");
    $objfrete = $buscafrete->fetch(PDO::FETCH_ASSOC);
    $valorfrete = $objfrete['valor'];
    foreach ($_SESSION['carrinho'] as $id => $qnt) {
        $consulta = $conexao->query("SELECT * FROM produtos WHERE id='$id'");
        $exibe = $consulta->fetch(PDO::FETCH_ASSOC);
        $nome_prod = $exibe['produto'];
        $preco = $exibe['preco'];
        $total += $preco * $qnt;
        $msgwpp .= $qnt . ' ' . $nome_prod .' - R$ '. number_format($preco, 2, ',', '.') . "%0A";
        $sql = "INSERT INTO vendas (ticket, id_comprador, id_produto, quantidade, data, valor,status,forma, id_frete, nome_comprador, endereco) VALUES ('$ticket',$id_user,'$id','$qnt','$data','$preco','A','$formapagto',$id_frete,'$comprador','$endereco')";
        $inserir = $conexao->query($sql);
        unset($_SESSION['carrinho']);
    }
    $total += $valorfrete;
    $msgwpp .= '_*FRETE*_' . "%0A";
    $msgwpp .= 'R$ ' . number_format($valorfrete, 2, ',', '.') . "%0A";
    $msgwpp .= '_*TOTAL*_' . "%0A";
    $msgwpp .= 'R$ ' . number_format($total, 2, ',', '.');
    limpaSessao();
    unset($_SESSION['forma']);
}

function limpaSessao() {
    unset($_SESSION['cidade']);
    unset($_SESSION['id_frete']);
    unset($_SESSION['nome_comprador']);
    unset($_SESSION['endereco']);
    unset($_SESSION['forma']);
}

if (!empty($_SESSION['id'])) {
    include 'fim.php';
    exit();
}
//var_dump($inserir);die();
header('location:'. "https://api.whatsapp.com/send?phone=554792811138&text={$msgwpp}");