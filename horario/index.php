<?php
session_start();
if (empty($_SESSION['adm']) || $_SESSION['adm'] != 1) {
    header('location:/sistema/index.php');
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Área Administrativa</title>
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/sistema/public/css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/animate.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font.css" type="text/css" cache="false">
        <link rel="stylesheet" href="/sistema/public/js/fuelux/fuelux.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/plugin.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/app.css" type="text/css">
    </head>
    <body>
        <?php
        include '../banco/conexao.php';
        $consulta = $conexao->query("SELECT * FROM horario;");
        ?>
        <section class="hbox stretch">
            <?php include '../template/menu.php'; ?>
            <section id="content">
                <section class="vbox">
                    <header class="header bg-light dker bg-gradient text-right">
                        <p class="pull-left">Lista de Horário</p>
                        <a href='inserir.php' class="btn btn-primary"><span class= "fa fa-pencil"></span> Cadastrar </a>
                    </header>
                    <section class="scrollable wrapper">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Horário</th>
                                    <th class="text-right">
                                        <i class="fal fa-cogs"></i>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php while ($exibe = $consulta->fetch(PDO::FETCH_ASSOC)) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $exibe['descricao']; ?>
                                        </td>
                                        <td class="text-right">
                                            <a href="editar.php?id=<?php echo $exibe['id_horario']; ?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                            <a href="svExcluir.php?id=<?php echo $exibe['id_horario']; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </section>
                </section>
            </section>
        </section>
        <script src="/sistema/public/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="/sistema/public/js/bootstrap.js"></script>
        <!-- App -->
        <script src="/sistema/public/js/app.js"></script>
        <script src="/sistema/public/js/app.plugin.js"></script>
        <script src="/sistema/public/js/app.data.js"></script>
        <!-- Fuelux -->
        <script src="/sistema/public/js/fuelux/fuelux.js"></script>
    </body>
</html>
