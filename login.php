<!DOCTYPE html>
<html>
    <head>
        <meta charset = "utf-8">
        <title> ACESSO </title>
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
        <script src="/sistema/public/js/modernizr.js"></script> <!-- Modernizr -->        
        <link rel="manifest" href="manifest.json">
        <link rel="icon" type="image/png" sizes="144x144"  href="/sistema/public/images/Icon-144.png">
        <link href="/sistema/public/images/Icon-144.png" rel="shortcut icon" type="image/vnd.microsoft.icon">
        <script src="/sistema/public/js/webapp.js"></script>
        <style type="text/css">
            body {
                font-family: "Lato", sans-serif;
            }



            .main-head{
                height: 150px;
                background: #FFF;

            }

            .sidenav {
                height: 100%;
                background-color: #000;
                overflow-x: hidden;
                padding-top: 20px;
            }


            .main {
                padding: 0px 10px;
            }

            @media screen and (max-height: 450px) {
                .sidenav {padding-top: 15px;}
            }

            @media screen and (max-width: 450px) {
                .login-form{
                    margin-top: 10%;
                }

                .register-form{
                    margin-top: 10%;
                }
            }

            @media screen and (min-width: 768px){
                .main{
                    margin-left: 40%; 
                }

                .sidenav{
                    width: 40%;
                    position: fixed;
                    z-index: 1;
                    top: 0;
                    left: 0;
                }

                .login-form{
                    margin-top: 80%;
                }

                .register-form{
                    margin-top: 20%;
                }
            }


            .login-main-text{
                margin-top: 20%;
                padding: 60px;
                color: #fff;
            }

            .login-main-text h2{
                font-weight: 300;
            }

            .btn-black{
                background-color: #000 !important;
                color: #fff;
            }
        </style>
    </head>
    <body>
        <?php
        include './banco/conexao.php';
        //include './template/nav.php';
        ?>

        <div class="sidenav">
            <div class="login-main-text text-center">
                <img class="img-circle" src="/sistema/assets/logoJeh1.png" width="50" alt="Homepage">
                <h2>Compre-Já<br> Nome da Loja </h2>
                <p>Logue ou registre-se para acessar o sistema.</p>
                <a href = "usuario/inserir.php" class="btn btn-sm btn-success">
                    <i class="fa fa-plus-circle"></i> Cadastre-se
                </a>
                <a class="btn btn-sm btn-warning" href="index.php"><i class="fa fa-reply"></i> Voltar a Loja</a>
            </div>
        </div>
        <div class="main">
            <div class="col-md-6 col-sm-12">
                <div class="login-form">
                    <form method = "post" action="autenticar.php" name = "logon">
                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="email" class="form-control" placeholder="E-mail" name="email">
                        </div>
                        <div class="form-group">
                            <label>Senha</label>
                            <input type="password" class="form-control" placeholder="Senha" name="senha">
                        </div>
                        <button type="submit" class="btn btn-black"><i class="fa fa-sign-in"></i> Login</button>
                        <a href = "usuario/recuperar.php" class="btn btn-info">
                            <i class="fa fa-user"></i> Esqueci a Senha
                        </a>	
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>