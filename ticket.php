<?php
session_start();
//if (empty($_SESSION['id'])) {
//    header('location:login.php');
//}
?>
<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Loja - Detalhes da Venda</title>
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
        <link rel="stylesheet" href="/sistema/style.css">
        <link rel="stylesheet" href="/sistema/public/css/reset.css"> <!-- CSS reset -->
        <link rel="stylesheet" href="/sistema/public/css/style.css"> <!-- Gem style -->
        <script src="/sistema/public/js/modernizr.js"></script> <!-- Modernizr -->

        <link rel="manifest" href="manifest.json">
        <link rel="icon" type="image/png" sizes="144x144"  href="/sistema/public/images/Icon-144.png">
        <link href="/sistema/public/images/Icon-144.png" rel="shortcut icon" type="image/vnd.microsoft.icon">
        <script src="/sistema/public/js/webapp.js"></script>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="timeline.css">
    </head>
    <body>	
        <?php
        include './banco/conexao.php';
        include './template/nav.php';
        $ticket_compra = $_GET['ticket'];
        $consultaVenda = $conexao->query("SELECT * FROM vendas WHERE ticket='$ticket_compra'");
        ?>

        <main class="container">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Pedido: <?php echo $ticket_compra ?></h3>
                    </div>
                    <div class="panel-body">
                        <?php
                        $total = 0;
                        while ($exibeVenda = $consultaVenda->fetch(PDO::FETCH_ASSOC)) {
                            $id_comprador = $exibeVenda['id_comprador'];
                            $frete = $exibeVenda['id_frete'];
                            $forma_pagto = $exibeVenda['forma'];
                            switch ($forma_pagto) {
                                case 'EC':
                                    $forma_pagto = 'Na entrega no Cartão';
                                    break;
                                case 'ED':
                                    $forma_pagto = 'Na entrega no Dinheiro';
                                    break;
                                case '0':
                                    $forma_pagto = 'Online';
                                    break;
                            }
                            $status_pedido = $exibeVenda['status'];
                            switch ($status_pedido) {
                                case 'C':
                                    $siglaStatus = "C";
                                    $status_pedido = 'Confirmado';
                                    break;
                                case 'A':
                                    $siglaStatus = "A";
                                    $status_pedido = 'Aguardando Confirmação';
                                    break;
                                case 'E':
                                    $siglaStatus = "E";
                                    $status_pedido = 'Saiu para Entrega';
                                    break;
                                case 'F':
                                    $siglaStatus = "F";
                                    $status_pedido = 'Entregue';
                                    break;
                            }
                            $total += $exibeVenda['valor'] * $exibeVenda['quantidade'];
                            ?>
                            <?php
                            $consultaProd = $conexao->query("SELECT produto FROM produtos WHERE id='$exibeVenda[id_produto]'");
                            $exibeProd = $consultaProd->fetch(PDO::FETCH_ASSOC);
                            if ($exibeProd['produto'] != '') {
                                $nome_produto = $exibeProd['produto'];
                            } else {
                                $nome_produto = 'Produto Excluído';
                            }
                            ?>                                
                            <blockquote>
                                <dl class="dl-horizontal">
                                    <dt>Produto</dt>
                                    <dd><?php echo $nome_produto; ?></dd>
                                    <dt>Quantidade</dt>
                                    <dd><?php echo $exibeVenda['quantidade']; ?></dd>
                                    <dt>Preço</dt>
                                    <dd>R$ <?php echo number_format($exibeVenda['valor'], 2, ',', '.'); ?></dd>
                                </dl>
                            </blockquote>
                        <?php } ?>
                        <dl class="dl-horizontal">
                            <dt>Forma de Pagamento</dt>
                            <dd><?php echo $forma_pagto; ?></dd>
                            <dt>Produtos</dt>
                            <dd>R$ <?php echo number_format($total, 2, ',', '.'); ?></dd>
                            <?php
                            $preco_frete = (isset($exibeFrete['valor'])) ? $exibeFrete['valor'] : 0.00;
                            if ($frete) {
                                $consultaFrete = $conexao->query("SELECT valor FROM frete WHERE id_frete=" . $frete);
                                $exibeFrete = $consultaFrete->fetch(PDO::FETCH_ASSOC);
                                $total = $total + $exibeFrete['valor'];
                                ?>
                                <dt>Frete:</dt>
                                <dd>R$  <?php echo number_format($exibeFrete['valor'], 2, ',', '.'); ?></dd>
                            <?php } ?>
                            <dt>Total deste Pedido</dt>
                            <dd>R$ <?php echo number_format($total, 2, ',', '.'); ?></dd>
                        </dl>
                    </div>
                    <?php if (isset($exibe_user['adm']) && $exibe_user['adm'] == 1) { // Se for Administrador ?>
                        <div class="panel-footer">
                            <div class="row">
                                <?php
                                if ($siglaStatus == 'A') {
                                    ?>
                                    <a class="btn btn-default" href='statusPedido.php?ticket=<?php echo $ticket_compra; ?>&status=C&id=<?php echo $id_comprador; ?>'><span class= "glyphicon glyphicon-road"></span>  Confirmar Pedido</a>
                                    <?php
                                } else if ($siglaStatus == 'C') {
                                    ?>
                                    <a  class="btn btn-default" href='statusPedido.php?ticket=<?php echo $ticket_compra; ?>&status=E&id=<?php echo $id_comprador; ?>'><span class= "glyphicon glyphicon-ok"></span>  Enviar para Entrega</a>
                                    <?php
                                } else if ($siglaStatus == 'E') {
                                    ?>
                                    <a  class="btn btn-default" href='statusPedido.php?ticket=<?php echo $ticket_compra; ?>&status=F&id=<?php echo $id_comprador; ?>'><span class= "glyphicon glyphicon-ok"></span>  Pedido Entregue</a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Status do Pedido</h3>
                    </div>
                    <div class="panel-body">


                        <ul class="timeline">
                            <?php if ($siglaStatus == 'A' || $siglaStatus == 'C' || $siglaStatus == 'E' || $siglaStatus == 'F') { ?>
                                <li>
                                    <div class="timeline-badge warning"><i class="glyphicon glyphicon-refresh"></i></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Aguardando</h4>
                                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> Pedido Realizado</small></p>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                            <?php if ($siglaStatus == 'C' || $siglaStatus == 'E' || $siglaStatus == 'F') { ?>
                                <li>
                                    <div class="timeline-badge success"><i class="glyphicon glyphicon-check"></i></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Pedido Aceito</h4>
                                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> Em preparação</small></p>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                            <?php if ($siglaStatus == 'E' || $siglaStatus == 'F') { ?>
                                <li>
                                    <div class="timeline-badge info"><i class="glyphicon glyphicon-bell"></i></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Retirado pelo entregador</h4>
                                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> Saiu para Entrega.</small></p>
                                        </div>

                                    </div>
                                </li>
                            <?php } ?>
                            <?php if ($siglaStatus == 'F') { ?>
                                <li>
                                    <div class="timeline-badge primary"><i class="glyphicon glyphicon-ok-sign"></i></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Entregue</h4>
                                            <p><small class="text-muted"><i class="glyphicon glyphicon-check"></i> Obrigado, volte sempre!.</small></p>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>

                        </ul>
                    </div>
                </div>
            </div>
        </main>
        <?php include './template/rodape.html' ?>
    </body>
</html>