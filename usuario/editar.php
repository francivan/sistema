<?php
session_start();
if (empty($_SESSION['id'])) {
    header('location:login.php');
}
$id_cliente = $_SESSION['id'];
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Área Administrativa</title>
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/sistema/public/css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/animate.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font.css" type="text/css" cache="false">
        <link rel="stylesheet" href="/sistema/public/js/fuelux/fuelux.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/plugin.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/app.css" type="text/css">

    </head>
    <body>
        <?php
        include '../banco/conexao.php';
        $consulta_user = $conexao->query("SELECT id_cliente,nome,sobrenome,email,senha,endereco,cidade,CEP FROM usuarios WHERE id_cliente='$id_cliente'");
        $exibe_user = $consulta_user->fetch(PDO::FETCH_ASSOC);
        ?>
        <section class="hbox stretch">
            <?php include '../template/menu.php'; ?>
            <section id="content">
                <section class="vbox">
                    <form method="post" action="svEditar.php?id=<?php echo $id_cliente ?>" name="alteraProd" enctype="multipart/form-data">
                        <header class="header bg-light dker bg-gradient text-right">
                            <p class="pull-left">Alterar Meus Dados</p>
                            <button type="submit" class="btn btn-primary">
                                <span class="fa fa-save"> Salvar </span>
                            </button>
                        </header>
                        <section class="scrollable wrapper">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="nome">Nome</label>
                                        <input name="nome" type="text" value="<?php echo $exibe_user['nome']; ?>" class="form-control" required id="nome">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="sobrenome">Sobrenome</label>
                                        <input name="sobrenome" type="text" value="<?php echo $exibe_user['sobrenome']; ?>" class="form-control" required id="sobrenome">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="email">E-mail</label>
                                        <input name="email" type="email" value="<?php echo $exibe_user['email']; ?>" class="form-control" required id="email">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="whatsapp">Whatsapp</label>
                                        <input name="whatsapp" value="<?php echo $exibe_user['whatsapp']; ?>" type="text" class="form-control" required id="whatsapp">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="senha">Senha</label>
                                        <input name="senha" type="password" value="<?php echo $exibe_user['senha']; ?>" class="form-control" required id="senha">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="cidade">Cidade</label>
                                        <!-- <input name="cidade" type="text" class="form-control" required id="cidade"> -->
                                        <select class="form-control" name="cidade" id="cidade" required>
                                            <?php
                                            $consulta_cidades = $conexao->query("SELECT id_cidade,nome from cidade");
                                            while ($exibe_cidades = $consulta_cidades->fetch(PDO::FETCH_ASSOC)) {
                                                ?>
                                                <option value="<?php echo $exibe_cidades['nome']; ?>" <?= ($exibe_user['cidade'] == $exibe_cidades['nome']) ? 'selected' : '' ?>><?php echo $exibe_cidades['nome']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="forma">Bairro / Taxa de Entrega</label>
                                        <select class="form-control" name="id_frete" id="bairro" required>
                                            <?php
                                            $consultaBairro = $conexao->query("SELECT a.id_frete, a.bairro, a.valor FROM frete a, cidade b WHERE a.id_cidade = b.id_cidade AND b.nome = '" . $exibe_user['cidade'] . "'");
                                            
                                            ?>
                                            <option value=""> Selecione</option>   
                                            <?php
                                            while ($exibe = $consultaBairro->fetch(PDO::FETCH_ASSOC)) {
                                                ?>
                                                <option value="<?= $exibe['id_frete']; ?>" <?= ($exibe_user['id_frete'] == $exibe['id_frete']) ? 'selected' : '' ?>> <?= $exibe['bairro']; ?>: R$ <?= $exibe['valor']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="endereco">Endereço</label>
                                        <input name="endereco" type="text" value="<?php echo $exibe_user['endereco']; ?>" class="form-control" required id="endereco">                                        
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="cep">CEP</label>
                                        <input name="cep" type="text" value="<?php echo $exibe_user['CEP']; ?>" class="form-control" required id="cep">
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </section>
            </section>
        </section>
    </section>
    <script src="/sistema/public/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/sistema/public/js/bootstrap.js"></script>
    <!-- App -->
    <script src="/sistema/public/js/app.js"></script>
    <script src="/sistema/public/js/app.plugin.js"></script>
    <script src="/sistema/public/js/app.data.js"></script>
    <!-- Fuelux -->
    <script src="/sistema/public/js/fuelux/fuelux.js"></script>
    <script src="../jquery.mask.js"></script>
    <script>
        $(document).ready(function () {
            $('#preco').mask('000.000.000.000.000,00', {reverse: true});
            $("#cidade").change(function(){
                var id = $(this).val();
                if (id) {
                    var url = './buscaBairro.php?id=' + id;
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {},
                        dataType: "html",
                        success: function (data) {
                            console.log(data);
                            $("#bairro").html(data);
                        }
                    });
                }
            });
        });
    </script>
</body>
</html>
