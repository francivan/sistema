<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <style type="text/css">
            body {
                font-family: "Lato", sans-serif;
            }



            .main-head{
                height: 150px;
                background: #FFF;

            }

            .sidenav {
                height: 100%;
                background-color: #000;
                overflow-x: hidden;
                padding-top: 20px;
            }


            .main {
                padding: 0px 10px;
            }

            @media screen and (max-height: 450px) {
                .sidenav {padding-top: 15px;}
            }

            @media screen and (max-width: 450px) {
                .login-form{
                    margin-top: 10%;
                }

                .register-form{
                    margin-top: 10%;
                }
            }

            @media screen and (min-width: 768px){
                .main{
                    margin-left: 40%; 
                }

                .sidenav{
                    width: 40%;
                    position: fixed;
                    z-index: 1;
                    top: 0;
                    left: 0;
                }

                .login-form{
                    margin-top: 80%;
                }

                .register-form{
                    margin-top: 20%;
                }
            }


            .login-main-text{
                margin-top: 20%;
                padding: 60px;
                color: #fff;
            }

            .login-main-text h2{
                font-weight: 300;
            }

            .btn-black{
                background-color: #000 !important;
                color: #fff;
            }
        </style>
        <script src="../jquery.mask.js"></script>
        <script>
            $(document).ready(function () {
                $("#cep").mask("00 000-000");
                $("#cidade").change(function () {
                    var id = $('#cidade').find(":selected").attr('data-target');
                    alert(id);
                    if (id) {
                        var url = '/sistema/buscaBairro.php?id=' + id;
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {},
                            dataType: "html",
                            success: function (data) {
                                $("#bairro").html(data);
                            }
                        });
                    }
                });
            });
        </script>
    </head>
    <body>
        <?php
        include '../banco/conexao.php';
        ?>

        <div class="sidenav">
            <div class="login-main-text text-center">
                <img class="img-circle" src="/sistema/assets/logoJeh1.png" width="50" alt="Homepage">
                <h2>Compre-Já<br> Nome da Loja </h2>
                <p>Registre-se para acessar o sistema.</p>
                <a href="/sistema/login.php" class="btn btn-sm btn-default">
                    <i class="fa fa-sign-in"></i> Acesso
                </a>
                <a class="btn btn-sm btn-warning" href="/sistema/index.php"><i class="fa fa-reply"></i> Voltar a Loja</a>
            </div>
        </div>
        <div class="main">
            <div class="col-md-6 col-sm-12">
                <div class="">
                    <h2>Cadastro de novo Cliente</h2>
                    <form method="post" action="svInserir.php" name="logon">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input name="nome" type="text" class="form-control" required id="nome">
                        </div>
                        <div class="form-group">
                            <label for="sobrenome">Sobrenome</label>
                            <input name="sobrenome" type="text" class="form-control" required id="sobrenome">
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input name="email" type="email" class="form-control" required id="email">
                        </div>
                        <div class="form-group">
                            <label for="whatsapp">Whatsapp</label>
                            <input name="whatsapp" type="text" class="form-control" required id="whatsapp">
                        </div>
                        <div class="form-group">
                            <label for="senha">Senha</label>
                            <input name="senha" type="password" class="form-control" required id="senha">
                        </div>
                        <div class="form-group">
                            <label for="endereco">Endereço</label>
                            <textarea name="endereco" rows="5" class="form-control" required id="endereco"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="cidade">Cidade</label>
                            <!-- <input name="cidade" type="text" class="form-control" required id="cidade"> -->
                            <select class="form-control" name="cidade" id="cidade" required>
                                <option value=""> Selecione</option>
                                <?php
                                $consulta_cidades = $conexao->query("SELECT id_cidade,nome from cidade");
                                while ($exibe_cidades = $consulta_cidades->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                                    <option data-target="<?php echo $exibe_cidades['id_cidade']; ?>" value="<?php echo $exibe_cidades['nome']; ?>"><?php echo $exibe_cidades['nome']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="forma">Bairro / Taxa de Entrega</label>
                            <select class="form-control" name="id_frete" id="bairro" required>
                                <option value=""> Selecione</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="cep">CEP</label>
                            <input name="cep" type="text" class="form-control" required id="cep">
                        </div>
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-save"></i> Cadastrar
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>