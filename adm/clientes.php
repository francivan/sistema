<?php
session_start();
include '../banco/conexao.php';
if (empty($_SESSION['id'])) {
    header('location:login.php');
} else {
    $consulta_user = $conexao->query("SELECT nome,adm FROM usuarios WHERE id_cliente='$_SESSION[id]'");
    $exibe_user = $consulta_user->fetch(PDO::FETCH_ASSOC);
    if ($exibe_user['adm'] != 1) {
        header('location:/sistema/index.php');
    }
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Área Administrativa</title>
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/sistema/public/css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/animate.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font.css" type="text/css" cache="false">
        <link rel="stylesheet" href="/sistema/public/js/fuelux/fuelux.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/plugin.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/app.css" type="text/css">
    </head>
    <body>
        <?php
        include '../banco/conexao.php';
        $id_user = $_SESSION['id'];
        $consulta_cliente = $conexao->query("SELECT * from usuarios where adm <> 1");
        ?>
        <section class="hbox stretch">
            <?php include '../template/menu.php'; ?>
            <section id="content">
                <section class="vbox">
                    <header class="header bg-light dker bg-gradient text-right">
                        <p class="pull-left">Lista de Produtos</p>
<!--                        <a href='inserir.php' class="btn btn-primary"><span class= "fa fa-pencil"></span> Cadastrar </a>-->
                    </header>
                    <section class="scrollable wrapper">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>CLIENTE</th>
                                    <th>CIDADE</th>
                                    <th>EMAIL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $qtdeclientes = 0;
                                while ($exibe_cliente = $consulta_cliente->fetch(PDO::FETCH_ASSOC)) {
                                    $qtdeclientes++;
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $exibe_cliente['nome'] . " " . $exibe_cliente['sobrenome']; ?>
                                        </td>
                                        <td>
                                           <?php echo $exibe_cliente['cidade']; ?>
                                        </td>
                                        <td>
                                            <?php echo $exibe_cliente['email']; ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <?php echo 'Total de Clientes: ' . $qtdeclientes; ?>
                    </section>
                </section>
            </section>
        </section>
        <script src="/sistema/public/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="/sistema/public/js/bootstrap.js"></script>
        <!-- App -->
        <script src="/sistema/public/js/app.js"></script>
        <script src="/sistema/public/js/app.plugin.js"></script>
        <script src="/sistema/public/js/app.data.js"></script>
        <!-- Fuelux -->
        <script src="/sistema/public/js/fuelux/fuelux.js"></script>
    </body>
</html>
