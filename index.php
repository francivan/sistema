<?php
session_start();
?>
<!doctype html>
<html lang = "pt-br">
    <head>
        <meta charset = "utf-8">
        <title> Loja </title>
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
        <link rel="stylesheet" href="/sistema/style.css">
        <link rel="stylesheet" href="/sistema/public/css/reset.css"> <!-- CSS reset -->
        <link rel="stylesheet" href="/sistema/public/css/style.css"> <!-- Gem style -->
        <script src="/sistema/public/js/modernizr.js"></script> <!-- Modernizr -->

        <link rel="manifest" href="manifest.json">
        <link rel="icon" type="image/png" sizes="144x144"  href="/sistema/public/images/Icon-144.png">
        <link href="/sistema/public/images/Icon-144.png" rel="shortcut icon" type="image/vnd.microsoft.icon">
        <script src="/sistema/public/js/webapp.js"></script>
    </head>
    <body>
        <?php
        include './banco/conexao.php';     // Conexão com o Banco
        $consulta = $conexao->query('SELECT * FROM produtos');
        ?>
        <?php include './template/nav.php'; ?>
        <?php include './template/cabecalho.html'; ?>
        <!-- PRIMEIRO TESTE DEPLOY -->
        <!-- FOOD MENU DE SELEÇÃO -->
        
        <main class="cd-main-content">
            <div class="cd-tab-filter-wrapper">
                <div class="cd-tab-filter">
                    <ul class="cd-filters">
                        <li class="placeholder"> 
                            <a data-type="all" href="#0">Todos</a> <!-- selected option on mobile -->
                        </li> 
                        <li class="filter"><a class="selected" href="#0" data-type="all">Todos</a></li>
                        <?php
                        $consulta_categorias = $conexao->query("SELECT codigo,descricao from categoria");
                        while ($exibe_categorias = $consulta_categorias->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                            <li class="filter" data-filter=".color-<?php echo $exibe_categorias['codigo']; ?>"><a href="#0" data-type="color-<?php echo $exibe_categorias['codigo']; ?>"><?php echo $exibe_categorias['descricao']; ?></a></li>
                        <?php } ?>  
                    </ul> <!-- cd-filters -->
                </div> <!-- cd-tab-filter -->
            </div> <!-- cd-tab-filter-wrapper -->

            <section class="cd-gallery">
                <ul>
                    <?php
                    while ($exibir = $consulta->fetch(PDO::FETCH_ASSOC)) {  // Enquanto tiver produtos vai criando as divs
                        ?>          
                        <li class="mix color-<?= $exibir['cod_categoria']; ?> food-item">                        
                            <div class="item-img">
                                <a href="./produto/informacao.php?id=<?php echo $exibir['id']; ?>">
                                    <div class="img-holder bg-img"style="background-image: url('/sistema/public/upload/<?php echo $exibir['foto1']; ?>');"></div>
                                </a>
                            </div>
                            <div class="item-info">
                                <div>
                                    <h3>
                                        <?php echo mb_strimwidth($exibir['produto'], 0, 13, '...'); ?>
                                    </h3>
                                    <span>
                                        R$ <?php echo number_format($exibir['preco'], 2, ',', '.'); ?>
                                    </span>
                                </div>
                                <?php if ($exibir['quantidade'] > 0) { ?>
                                    <a href = "carrinho.php?id=<?php echo $exibir['id']; ?>&action=adicionar" class="cart-btn">
                                        <i class="bx bx-cart-alt"></i>
                                    </a>
                                <?php } else { ?>
                                    <a href = "#" class="cart-btn" disabled>
                                        <i class="bx bx-cart-alt"></i> Indisponível
                                    </a>
                                <?php } ?>
                            </div>
                        </li>
                    <?php } ?>  
                    <li class="gap"></li>
                    <li class="gap"></li>
                    <li class="gap"></li>
                </ul>
                <div class="cd-fail-message">Nenhum produto cadastrado</div>
            </section> <!-- cd-gallery -->

            <div class="cd-filter">
                <form>
                    <div class="cd-filter-block">
                        <h4>Search</h4>

                        <div class="cd-filter-content">
                            <input type="search" placeholder="Try color-1...">
                        </div> <!-- cd-filter-content -->
                    </div> <!-- cd-filter-block -->

                    <div class="cd-filter-block">
                        <h4>Check boxes</h4>

                        <ul class="cd-filter-content cd-filters list">
                            <li>
                                <input class="filter" data-filter=".check1" type="checkbox" id="checkbox1">
                                <label class="checkbox-label" for="checkbox1">Option 1</label>
                            </li>

                            <li>
                                <input class="filter" data-filter=".check2" type="checkbox" id="checkbox2">
                                <label class="checkbox-label" for="checkbox2">Option 2</label>
                            </li>

                            <li>
                                <input class="filter" data-filter=".check3" type="checkbox" id="checkbox3">
                                <label class="checkbox-label" for="checkbox3">Option 3</label>
                            </li>
                        </ul> <!-- cd-filter-content -->
                    </div> <!-- cd-filter-block -->

                    <div class="cd-filter-block">
                        <h4>Select</h4>

                        <div class="cd-filter-content">
                            <div class="cd-select cd-filters">
                                <select class="filter" name="selectThis" id="selectThis">
                                    <option value="">Choose an option</option>
                                    <option value=".option1">Option 1</option>
                                    <option value=".option2">Option 2</option>
                                    <option value=".option3">Option 3</option>
                                    <option value=".option4">Option 4</option>
                                </select>
                            </div> <!-- cd-select -->
                        </div> <!-- cd-filter-content -->
                    </div> <!-- cd-filter-block -->
                </form>

                <a href="#0" class="cd-close">Close</a>
            </div> <!-- cd-filter -->

        </main> <!-- cd-main-content -->

        <script src="public/js/jquery-2.1.1.js"></script>
        <script src="public/js/jquery.mixitup.min.js"></script>
        <script src="public/js/main.js"></script> <!-- Resource jQuery -->
        <!-- FIM DO MENU FOOD -->
        <script src="/sistema/public/js/main.js"></script> <!-- Gem jQuery -->
        <!-- END TESTIMONIAL SECTION -->
        <?php include './template/cart.php'; ?>

    </body>
</html>
