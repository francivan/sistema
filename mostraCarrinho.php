
    <div class="row text-center" style="margin-top: 15px;">
        <h1>Carrinho de Compras</h1>
    </div>
    <?php
    if (!isset($_SESSION['carrinho'])) {
        $_SESSION['carrinho'] = array();
    }
    $total = null;
    ?>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Produtos</h3>
        </div>
        <div class="panel-body">
            <?php
            $produtos = [];
            foreach ($_SESSION['carrinho'] as $id => $qnt) {


                $consulta = $conexao->query("SELECT * FROM produtos WHERE id='$id'");
                $exibe = $consulta->fetch(PDO::FETCH_ASSOC);
                $produto = $exibe['produto'];
                $preco = number_format(($exibe['preco']), 2, ',', '.');
                $total += $exibe['preco'] * $qnt;
                if ($qnt == 0) {
                    echo '<script>location.href=\'removeCarrinho.php?id=' . $id . '\';</script>';
                }
                ?>

                <div class="col-md-12">
                    <div class="col-sm-1 col-sm-offset-1">
                        <img src="/sistema/public/upload/<?php echo $exibe['foto1']; ?>" class="img-rounded img-responsive">
                    </div>
                    <div class="col-sm-4">
                        <h4><?php echo $produto; ?></h4>
                    </div>	
                    <div class="col-sm-2">
                        <h4>Unitário: R$ <?php echo $preco; ?></h4>
                    </div>		
                    <div class="col-sm-2">
                        <h4>Qtde: <?php echo $qnt; ?> </h4>
                    </div>
                    <div class="col-sm-2">
                        <a href="carrinho.php?id=<?php echo $id; ?>&action=adicionar" class="btn btn-sm  btn-primary">	
                            <span class="glyphicon glyphicon-plus"></span>		
                        </a>
                        <a href="carrinho.php?id=<?php echo $id; ?>&action=remover" class="btn btn-sm  btn-primary">	
                            <span class="glyphicon glyphicon-minus"></span>		
                        </a>                
                        <a href="removeCarrinho.php?id=<?php echo $id; ?>" class="btn btn-sm btn-danger">	
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div> 
                </div>

                <?php
                $produtos[] = $produto . ' R$ ' . $preco;
            }
            ?>
        </div>
        <div class="panel-footer text-right">
            <input type="hidden" id="totalProduto" value="<?= number_format($total, 2, ',', '.'); ?>" />
            <h2>FRETE: R$ <span id="valorFrete"><?php (isset($_SESSION['frete'])) ? print number_format($_SESSION['frete'], 2, ',', '.') : print '0,00'; ?></span></h2>
        </div>
    </div>
    <?php if (empty($_SESSION['id'])) { ?>   
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Local de Entrega</h3>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="nome_cliente">Nome</label>
                        <input name="nome_comprador" placeholder="Nome Sobrenome" type="text" class="form-control" required id="nome_comprador" value="<?php (isset($_SESSION['nome_comprador'])) ? print $_SESSION['nome_comprador'] : print ''; ?>">
                    </div>  
                </div>
                <div class="col-md-6">
                    <label for="forma">Cidade</label>
                    <select class="form-control" name="cidade" id="cidade" required>
                        <option value=""> Selecione</option>
                        <?php
                        $consulta_cidades = $conexao->query("SELECT id_cidade,nome from cidade");
                        while ($exibe_cidades = $consulta_cidades->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                        <option value="<?php echo $exibe_cidades['id_cidade']; ?>" <?php (isset($_SESSION['cidade']) && $_SESSION['cidade'] == $exibe_cidades['id_cidade']) ? print 'selected' : print ''; ?>><?php echo $exibe_cidades['nome']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="forma">Bairro / Taxa de Entrega</label>
                    <select class="form-control" name="id_frete" id="bairro" required>
                        <option value=""> Selecione</option>
                        <?php
                        if (isset($_SESSION['cidade'])) {
                            $consulta = $conexao->query("SELECT * FROM frete WHERE id_cidade = " . $_SESSION['cidade']);
                            while ($exibe_frete = $consulta->fetch(PDO::FETCH_ASSOC)) {
                                ?>
                                <option value="<?= $exibe_frete['id_frete']; ?>" <?php ($_SESSION['id_frete'] && $_SESSION['id_frete'] == $exibe_frete['id_frete']) ? print 'selected' : print ''; ?>> <?= $exibe_frete['bairro']; ?>: R$ <?= number_format($exibe_frete['valor'], 2, ',', '.'); ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="endereco">Endereço </label>
                        <input placeholder="Rua explemplo, 000, Ponto de referência: Proximo ao terminal sul" name="endereco" type="text" class="form-control" required id="endereco" value="<?php (isset($_SESSION['endereco'])) ? print $_SESSION['endereco'] : print ''; ?>">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="obs">Observação</label>
                        <textarea name="obs" rows="5" placeholder="Ex: Retirar o alface" class="form-control" id="obs"><?php (isset($_SESSION['obs'])) ? print $_SESSION['obs'] : print ''; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
        <script>
                $(document).ready(function () {
                    $("#cidade").change(function(){
                        var id = $(this).val();
                        if (id) {
                            var url = './buscaBairro.php?id=' + id;
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: {},
                                dataType: "html",
                                success: function (data) {
                                    $("#bairro").html(data);
                                }
                            });
                        }
                    });
                                
                                
                     $("#bairro").change(function(){
                        var id = $(this).val();
                        var total_pagar = parseFloat($("#totalProduto").val().replace(',', '.'));
                        var texto = $("#bairro option:selected").text().replace(',', '.');
                        var valor = texto.split("R$");
                        var soma = total_pagar + parseFloat(valor[1]);
                        var total = soma.toFixed(2);
                        if (parseFloat(valor[1])) {
                            var url = './updateFrete.php?id='+ id +'&valor=' + parseFloat(valor[1]);
                            $.ajax({
                                type: "GET",
                                url: url,
                                data: {},
                                dataType: "html",
                                success: function (data) {
                                    $('#valorFrete').html(data.replace('.', ','));
                                    $('#total_pagar').html(total.replace('.', ','));
                                }
                            });
                        }
                    });
                });
        </script>
        <?php
    }else {
        ?>
        <input type="hidden" name="id_frete" value="<?= $_SESSION['id_frete']; ?>">
        <?php
    }
    ?>