<script type="text/javascript">
    $(document).on('click', '.btn-notification', function () {
        $('.btn-notification').removeClass('active');
        $(this).addClass('active');
    });
</script>
<header>
    <div id="logo"><img src="/sistema/assets/logoJeh1.png" width="50" alt="Homepage"></div>
    <div id="cd-hamburger-menu"><a class="cd-img-replace" href="/sistema/index.php">Menu</a></div>
    <div id="cd-cart-trigger"><a class="cd-img-replace" href="#0">Cart</a></div>
</header>
<nav id="main-nav">
    <ul>
        <li><a class="current" href="/sistema/index.php">Home</a></li>
        <?php if (empty($_SESSION['id'])) { ?>
            <li><a href="/sistema/login.php">Login</a></li>
            <?php
        } else {
            $consulta_user = $conexao->query("SELECT a.nome,a.adm, a.endereco, a.cidade, b.bairro,b.id_frete, b.valor FROM usuarios a, frete b WHERE a.id_frete = b.id_frete AND a.id_cliente=" . $_SESSION['id']);
            $exibe_user = $consulta_user->fetch(PDO::FETCH_ASSOC);
            if (isset($exibe_user['valor'])) {
                $_SESSION['frete'] = $exibe_user['valor'];
                $_SESSION['id_frete'] = $exibe_user['id_frete'];
            }
            if ($exibe_user['adm'] == 0) { // se n for um Administrador
                ?>
                <li>
                    <a href="/sistema/usuario/editar.php"><?php echo $exibe_user['nome']; ?> : <?php echo $exibe_user['endereco'] . " - " . $exibe_user['bairro'] . " - " . $exibe_user['cidade']; ?></a>
                </li>
                <li>
                    <a href="#" class="btn-notification" id="inscrevase">
                        <i class="fa fa-bell"></i>
                    </a>
                </li>
                <li>
                    <a href="/sistema/sair.php">
                        Sair
                    </a>
                </li>
                <input type="hidden" name="id_cliente_logado" id="id_cliente_logado"  value="<?= $_SESSION['id']; ?>"/>
            <?php } else { ?>
                <li><a href="/sistema/adm/adm.php">ADMINISTRAÇÃO</a></li>
                <li>
                    <a href="/sistema/sair.php">
                        Sair
                    </a>
                </li>
                <li>
                    <div class="btn-notification" id="inscrevase">
                        <i class="fa fa-bell"></i>
                    </div>
                </li>
                <input type="hidden" name="id_cliente_logado" id="id_cliente_logado"  value="<?= $_SESSION['id']; ?>"/>
                <?php
            }
        }
        ?>
    </ul>
</nav>
