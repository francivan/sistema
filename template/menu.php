<?php
$consulta_user = $conexao->query("SELECT * FROM usuarios WHERE id_cliente='$_SESSION[id]'");
$exibe_user = $consulta_user->fetch(PDO::FETCH_ASSOC);
                
?>
<aside class="bg-black dker aside-sm nav-vertical" id="nav">
    <section class="vbox">
        <header class="bg-black nav-bar">
            <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav">
                <i class="fa fa-bars"></i>
            </a>
            <a href="/sistema/index.php" class="nav-brand" data-toggle="fullscreen">SL</a>
            <a class="btn btn-link visible-xs" data-toggle="collapse" data-target=".navbar-collapse">
                <i class="fa fa-comment-o"></i>
            </a>
        </header>
        <section>
            <nav class="nav-primary hidden-xs">
                <ul class="nav">
                    <?php 
                    if ($exibe_user['adm'] == 1) { 
                    ?>
                    <li>
                        <a href="/sistema/adm/adm.php">
                            <i class="fa fa-home"></i>
                            <span>Home</span>
                        </a>
                    </li>              
                    <li>
                        <a href="/sistema/produto/index.php">
                            <i class="fa fa-cutlery"></i>
                            <span>Produto</span>
                        </a>
                    </li>
                    <li>
                        <a href="/sistema/cidade/index.php">
                            <i class="fa fa-map-marker"></i>
                            <span>Cidade</span>
                        </a>
                    </li>
                    <li>
                        <a href="/sistema/usuario/index.php">
                            <i class="fa fa-list-alt"></i>
                            <span>Pedidos</span>
                        </a>
                    </li>
                    <li>
                        <a href="/sistema/categoria/index.php">
                            <i class="fa fa-list"></i>
                            <span>Categorias</span>
                        </a>
                    </li>
                    <li>
                        <a href="/sistema/adm/clientes.php">
                            <i class="fa fa-users"></i>
                            <span>Clientes</span>
                        </a>
                    </li>
                    <li>
                        <a href="/sistema/adm/vendas.php">
                            <i class="fa fa-credit-card"></i>
                            <span>Vendas</span>
                        </a>
                    </li>
                    <li>
                        <a href="/sistema/frete/index.php">
                            <i class="fa fa-truck"></i>
                            <span>Frete</span>
                        </a>
                    </li>
                    <li>
                        <a href="/sistema/horario/index.php">
                            <i class="fa fa-clock-o"></i>
                            <span>Horário</span>
                        </a>
                    </li>
                    <li>
                        <a href="/sistema/usuario/editar.php">
                            <i class="fa fa-user"></i>
                            <span>Meu Perfil</span>
                        </a>
                    </li>
                    <?php 
                    } else {
                    ?>
                    <li>
                        <a href="/sistema/adm/adm.php">
                            <i class="fa fa-home"></i>
                            <span>Loja</span>
                        </a>
                    </li> 
                    <li>
                        <a href="/sistema/usuario/index.php">
                            <i class="fa fa-list-alt"></i>
                            <span>Pedidos</span>
                        </a>
                    </li>
                    <li>
                        <a href="/sistema/usuario/editar.php">
                            <i class="fa fa-user"></i>
                            <span>Meu Perfil</span>
                        </a>
                    </li>
                    <?php 
                    } 
                    ?>
                </ul>
            </nav>
            <!-- / nav -->
        </section>
        <footer class="footer bg-gradient hidden-xs">
            <a href="/sistema/sair.php" class="btn btn-sm btn-link m-r-n-xs pull-right">
                <i class="fa fa-power-off"></i>
            </a>
            <a href="#nav" data-toggle="class:nav-vertical" class="btn btn-sm btn-link m-l-n-sm">
                <i class="fa fa-bars"></i>
            </a>
        </footer>
    </section>
</aside>