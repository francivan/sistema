<?php
if (!isset($_SESSION['carrinho'])) {
    $_SESSION['carrinho'] = array();
}
$total = null;
?>
<div id="cd-shadow-layer"></div>

<div id="cd-cart">
    <h2>Carrinho</h2>
    <ul class="cd-cart-items">
        <?php
        $produtos = [];
        foreach ($_SESSION['carrinho'] as $id => $qnt) {
            $consulta = $conexao->query("SELECT * FROM produtos WHERE id='$id'");
            $exibe = $consulta->fetch(PDO::FETCH_ASSOC);
            $produto = $exibe['produto'];
            $preco = number_format(($exibe['preco']), 2, ',', '.');
            $total += $exibe['preco'] * $qnt;
            if ($qnt == 0) {
                echo '<script>location.href=\'removeCarrinho.php?id=' . $id . '\';</script>';
            }
            ?>
            <li>
<!--                <img src="/sistema/public/upload/<?php echo $exibe['foto1']; ?>" width="50px" class="img-rounded">-->
                <span class="cd-qty"><?php echo $qnt; ?>x</span> <?php echo $produto; ?>
                <div class="cd-price">R$ <?php echo $preco; ?></div>
                <a href="removeCarrinho.php?id=<?php echo $id; ?>" class="cd-item-remove cd-img-replace"><span class="glyphicon glyphicon-remove"></span></a>
            </li>
            <?php
            $produtos[] = $produto . ' R$ ' . $preco;
        }
        if (!empty($_SESSION['id']) && $exibe_user['valor']) {
            $total += $exibe_user['valor'];
        } elseif (!isset($_SESSION['id']) && isset($_SESSION['frete'])) {
            $total += $_SESSION['frete'];
        }
        ?>
    </ul> <!-- cd-cart-items -->

    <div class="cd-cart-total">
        <p>FRETE R$ <span id="valorFrete"><?php (isset($_SESSION['frete'])) ? print number_format($_SESSION['frete'], 2, ',', '.') : print '0,00'; ?></span></p>
    </div> <!-- cd-cart-total -->
    <div class="cd-cart-total">
        <p>TOTAL  R$ <span id="total_pagar"><?php echo number_format($total, 2, ',', '.'); ?></p>
    </div> <!-- cd-cart-total -->

    <a href="/sistema/carrinho.php" class="checkout-btn">Finalizar Compra</a>

    <p class="cd-go-to-cart"><a href="/sistema/index.php">Continuar Comprando</a></p>
</div> <!-- cd-cart -->

<script src="/sistema/public/js/main.js"></script> <!-- Gem jQuery -->