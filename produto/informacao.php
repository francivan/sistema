<?php
session_start();
?>
<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Loja</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
        <link rel="stylesheet" href="../style.css">
        <link rel="stylesheet" href="/sistema/public/css/reset.css"> <!-- CSS reset -->
        <link rel="stylesheet" href="/sistema/public/css/style.css"> <!-- Gem style -->
        <script src="/sistema/public/js/modernizr.js"></script> <!-- Modernizr -->        
        <link rel="manifest" href="manifest.json">
        <link rel="icon" type="image/png" sizes="144x144"  href="/sistema/public/images/Icon-144.png">
        <link href="/sistema/public/images/Icon-144.png" rel="shortcut icon" type="image/vnd.microsoft.icon">
        <script src="/sistema/public/js/webapp.js"></script>
    </head>
    <body>	
        <?php
        include '../banco/conexao.php';
        include '../template/nav.php';
        if (!empty($_GET['id'])) {
            $id_prod = $_GET['id'];
            $consulta = $conexao->query("SELECT * FROM produtos where id = '$id_prod'");
            $exibir = $consulta->fetch(PDO::FETCH_ASSOC);
        } else {
            header('location:/sistema/index.php');
        }
        ?>
        <div class="row">
            <div class="col-sm-6 col-md-4">

            </div>
        </div>
        <main class="container">
            <div class="row">
                <div class="col-sm-5 col-sm-offset-3">
                    <h1>Detalhes do Produto</h1>

                    <div class="thumbnail">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="/sistema/public/upload/<?php echo $exibir['foto1']; ?>" class="img-responsive" style="width:100%;">
                                    <div class="carousel-caption">
                                        Foto
                                    </div>
                                </div>
                                <?php if ($exibir['foto2'] != '') { ?>
                                    <div class="item">
                                        <img src="/sistema/public/upload/<?php echo $exibir['foto2']; ?>" class="img-responsive" style="width:100%;">
                                        <div class="carousel-caption">
                                            ...
                                        </div>
                                    </div>
                                    <?php
                                }
                                if ($exibir['foto3'] != '') {
                                    ?>
                                    <div class="item">
                                        <img src="/sistema/public/upload/<?php echo $exibir['foto3']; ?>" class="img-responsive" style="width:100%;">
                                        <div class="carousel-caption">
                                            ...
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <div class="caption">
                            <h1><?php echo $exibir['produto']; ?></h1>
                            <p><?php echo nl2br($exibir['descricao']); ?></p>
                        <!-- <p><?php //echo $exibir['marca'];                                 ?></p> -->
                            <p>R$ <?php echo number_format($exibir['preco'], 2, ',', '.'); ?></p>
                            <a href = "../carrinho.php?id=<?php echo $exibir['id']; ?>&action=adicionar">
                                <button class="btn btn btn-success">Comprar</button>
                            </a>
                        </div>
                    </div>	
                </div>
            </div>
        </main>
        <?php
        include '../template/rodape.html';
        ?>
    </body>
</html>