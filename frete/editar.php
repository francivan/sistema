<?php
session_start();
if (empty($_SESSION['adm']) || $_SESSION['adm'] != 1) {
    header('location:/sistema/index.php');
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Cadastro de Frete</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/sistema/public/css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/animate.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/font.css" type="text/css" cache="false">
        <link rel="stylesheet" href="/sistema/public/js/fuelux/fuelux.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/plugin.css" type="text/css">
        <link rel="stylesheet" href="/sistema/public/css/app.css" type="text/css">

    </head>
    <body>
        <?php
        $id_frete = $_GET['id'];
        include '../banco/conexao.php';
        $consulta = $conexao->query("SELECT * from frete WHERE id_frete ='$id_frete'");
        $exibe = $consulta->fetch(PDO::FETCH_ASSOC)
        ?>
        <section class="hbox stretch">
            <?php include '../template/menu.php'; ?>
            <section id="content">
                <section class="vbox">
                    <form method="post" action="svEditar.php?id=<?php echo $id_frete; ?>" name="alteraProd" enctype="multipart/form-data">
                        <input type="hidden" name="id_frete" value="<?php echo $id_frete; ?>" />
                        <header class="header bg-light dker bg-gradient text-right">
                            <p class="pull-left">Cadastro de Frete</p>
                            <button type="submit" class="btn btn-primary">
                                <span class="fa fa-save"> Salvar </span>
                            </button>
                        </header>
                        <section class="scrollable wrapper">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="bairro">Bairro</label>
                                        <input name="bairro"  value="<?php echo $exibe['bairro']; ?>" type="text" class="form-control" required id="bairro" style="text-transform: uppercase;">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group ">
                                        <label class="control-label"for="id_cidade">Tipo *</label>                                    
                                        <select class="form-control" name="id_cidade">
                                            <option value="">Selecione</option>
                                            <?php
                                            $consulta_categorias = $conexao->query("SELECT * from cidade");
                                            while ($exibe_categorias = $consulta_categorias->fetch(PDO::FETCH_ASSOC)) {
                                                ?>
                                            <option <?php ($exibe_categorias['id_cidade'] == $exibe['id_cidade']) ? print "selected" : print ""; ?> value="<?php echo $exibe_categorias['id_cidade']; ?>" ><?php echo $exibe_categorias['nome']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>                                
                                    </div>
                                </div>       
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="valor">Preço</label>
                                        <input type="text" value="<?php echo $exibe['valor']; ?>" class="form-control" required name="valor" id="valor">
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </section>
            </section>
        </section>

    </section>
    <script src="/sistema/public/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/sistema/public/js/bootstrap.js"></script>
    <!-- App -->
    <script src="/sistema/public/js/app.js"></script>
    <script src="/sistema/public/js/app.plugin.js"></script>
    <script src="/sistema/public/js/app.data.js"></script>
    <!-- Fuelux -->
    <script src="/sistema/public/js/fuelux/fuelux.js"></script>
    <script src="../jquery.mask.js"></script>
    <script>
        $(document).ready(function () {
            $('#valor').mask('000.000.000.000.000,00', {reverse: true});
        });
    </script>
</body>
</html>
